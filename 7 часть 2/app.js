const express = require('express');
const app = express();
app.listen(3000);

const admin = require('./essence/admin');
const user = require('./essence/user');

global.user = {
};

global.user.id = process.argv[2];
global.user.name = process.argv[3];

app.get('/me', function(req, res){
    res.send('<h1>Информация:</h1><p>id=' + global.user.id + '</p><p>name=' + global.user.name + '</p>')
});

app.get('/', function(req, res){
    res.send('<h1>Главная страница</h1>');
});


app.use(function(req, res, next){
    if ((global.user.id && global.user.name) || (process.argv[2] && process.argv[3]))
    res.send('<h1>Данные уже внесены</h1>');
    else next();
});

app.get('/about', function(req, res){
    global.user.id = req.query.id;
    global.user.name = req.query.name;
    res.send('<h1>Получение данных</h1>');
});

app.use('/', admin, user)