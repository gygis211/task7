const express = require('express');
const router = express();
const bodyParser = require('body-parser')
const urlencodedParser = bodyParser.urlencoded({extends: true})


router.post('/user/create', urlencodedParser, (req, res)=>{
    global.user = {
        id: req.body.id,
        userName: req.body.userName
    };
    res.send(`<h1>User has been created</h1>
    <p>id: ${global.user.id}</p>
    <p>name: ${global.user.userName}</p>`)
});

router.get('/user/:id', urlencodedParser, (req, res)=>{
    res.send(`<h1>User name: ${global.user.userName}</h1>`)
});

router.put('/user/:id', urlencodedParser, (req, res)=>{
    global.user.userName = req.body.userName
    res.send(`<h1>User changet</h1>
    <p>${global.user.userName}</p>`)
});

router.delete('/user/:id', urlencodedParser, (req, res)=>{
    res.send('<h1>User deleted</h1>')
});

module.exports = router;