const express = require('express');
const router = express();
const bodyParser = require('body-parser');
const urlencodedParser = bodyParser.urlencoded({extended: true});

global.admin = {};

router.post('/admin/create', urlencodedParser,(req, res)=>{
    global.admin.id = req.body.id
    global.admin.adminName = req.body.adminName
    res.send(`<h1>Admin has been created</h1>
    <p>id: ${global.admin.id}</p>
    <p>name: ${global.admin.adminName}</p>`)
});

router.get('/admin/:id', urlencodedParser,(req, res)=>{
    res.send(`<h1>Admin name: ${global.admin.adminName}</h1>`)
});

router.put('/admin/:id', urlencodedParser,(req, res)=>{
    global.admin.adminName = req.body.adminName
    res.send(`<h1>User changed</h1>
    <p>New name: ${global.admin.adminName}</p>`)
});

router.delete('/admin/:id', urlencodedParser,(req, res)=>{
    res.send(`<h1>User deleted</h1>`)
});

module.exports = router;