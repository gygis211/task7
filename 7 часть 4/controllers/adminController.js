const Admin = require("../model/admin.js");

exports.addAdmin = (req, res)=>{
    const adminName = req.body.name;
    const id = req.body.id;
    const admin = new Admin(adminName, id);
    admin.save();
    res.send(`<h1>Admin has been created</h1>
    <p>id: ${admin.id}</p>
    <p>name: ${admin.adminName}</p>`)
};

exports.getAdmins = (req, res)=>{
    res.send(`<h1>Admin name: ${global.admin.adminName}</h1>`)
};
 
exports.deleteAdmin = (req, res)=>{
    global.admin.adminName = req.body.adminName
    res.send(`<h1>User changed</h1>
    <p>New name: ${global.admin.adminName}</p>`)
}

exports.putAdmin = (req, res)=>{
    res.send(`<h1>User deleted</h1>`)
};