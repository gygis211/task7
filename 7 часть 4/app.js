const express = require('express');
const router = express.Router();
const app = express();
const bodyParser = require('body-parser');

const userRoutes = require('./routes/userRouter');
const adminRoutes = require('./routes/adminRouter');

app.use(bodyParser.urlencoded({extended: true}));
app.use('/user', userRoutes);
app.use('/admin', adminRoutes);


app.listen(3000);