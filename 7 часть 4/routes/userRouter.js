const express = require("express");
const userController = require("../controllers/userController.js");
const userRouter = express.Router();
 
userRouter.post("/create", userController.addUser);
userRouter.get("/:id", userController.getUsers);
userRouter.put("/:id", userController.putUser);
userRouter.delete("/:id", userController.deleteUser);
 
module.exports = userRouter; 