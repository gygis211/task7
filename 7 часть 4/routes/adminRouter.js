const express = require("express");
const adminController = require("../controllers/adminController.js");
const adminRouter = express.Router();

adminRouter.post("/create", adminController.addAdmin);
adminRouter.get("/:id", adminController.getAdmins);
adminRouter.put("/:id", adminController.deleteAdmin);
adminRouter.delete("/:id", adminController.putAdmin);
 
module.exports = adminRouter;