const admin = [];
 
module.exports= class Admin{
 
    constructor(name, id){
        this.name = name;
        this.id = id;
    }
    save(){
        admin.push(this);
    }
    static getAll(){
        return admin;
    }
}